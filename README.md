# tools

This repo contains the scripts that are used to evaluate Aevol's performances, and maybe other tools written for convenience.

# iobench

iobench is a script that run aevol_run and extract the logger.csv file generated to trace its IO operations.

## usage :

`./iobench.sh <list of number of thread> <list of end generations>`

ex : `./iobench.sh "1 2 3 4 5" "100 1000 10000"`

Results are stored in `iobench/results` and named `pXgY.csv` where X is the number of thread and Y the end generation given to aevol_run.
