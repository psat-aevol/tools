#! /bin/bash

core_list=$1
generation_list=$2
date_format="+%F:%Hh%Mm%Ss"
logger_file="new_logger.csv"

if [ ! -d "iobench" ]; then
    echo "Creating directories."
    mkdir iobench
    mkdir iobench/world_backup
    mkdir iobench/results
    
    cd iobench/world_backup
    echo "Creating an aevol world for experience."
    aevol_create -f ../../param.in > log.out
    echo -n "Running until generation 50000..."
    aevol_run -p 8 -e 50001 >> log.out
    echo "Done !"
    mv $logger_file ../results/world_creation_log.csv
    cd ..
else
    echo "iobench directory already exist. Skipping directories and world creation."
    cd ./iobench
fi

for core in $core_list
do
    for generation in $generation_list
    do
        cp -R world_backup world
        cd world

        date=`date ${date_format}`
        echo -n -e "[${date}] Experience core : $core, generation end : $generation... "
        
        aevol_run -p $core -e $generation > log.out
        
        date=`date ${date_format}`
        echo "Done ! (finished at [${date}])"
        mv $logger_file "../results/p${core}g${generation}.csv"
        cd ..
        rm -rf world
    done 
done 

