# zlib bench

This folder contains cpp files and R markdown file for testing `gzwrite` performances.

## Usage

Compile & execute the `groupedWrite` executable, then open the `data_pres.rmd` file with RStudio and generate a pdf file.

Compile & execute with the following commands :

```
make
./groupedWrite
```

The csv files will be stored in the `data` folder.
