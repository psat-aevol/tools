#include <iostream>
#include <fstream>
#include <chrono>

#include <zlib.h>

#include "groupedWrites.h"

using namespace std;

int main(int argc, char ** argv)
{
    testSmallStructWriting();
    testBigStructWriting();
    dynamicTest();
    testHugeArrayWriting();
}

void testSmallStructWriting()
{
    unsigned int n = 1000;
    writeSmallStruct(n);
    writeThreeInts(n);
}

void writeSmallStruct(unsigned int n_times)
{
    ofstream csvfile("data/smallstruct.csv");
    csvfile << "size of struct, time (nanoseconds)" << endl;

    gzFile file = gzopen("data/smallstructdump", "w");
    unsigned int length = sizeof(smallStruct);
    for(unsigned int i = 0; i < n_times; ++i)
    {
        smallStruct s;
        
        auto write_start = chrono::high_resolution_clock::now();
        gzwrite(file, &s, length);
        auto write_end = chrono::high_resolution_clock::now();
        long duration = chrono::duration_cast<chrono::nanoseconds>( write_end - write_start ).count();
        csvfile << to_string(length) << ',' << to_string(duration) << endl;
    }
    gzclose(file);
}

void writeThreeInts(unsigned int n_times)
{
    ofstream csvfile("data/3_ints.csv");
    csvfile << "size of write, number of writes, time (nanoseconds)" << endl;
    gzFile file = gzopen("data/3_intsdump", "w");
    unsigned int length = sizeof(int16_t);
    unsigned int n_writes = 3;
    for(unsigned int i = 0; i < n_times; ++i)
    {
        int16_t a, b, c;
        
        auto write_start = chrono::high_resolution_clock::now();
        
        gzwrite(file, &a, length);
        gzwrite(file, &b, length);
        gzwrite(file, &c, length);
        
        auto write_end = chrono::high_resolution_clock::now();
        long duration = chrono::duration_cast<chrono::nanoseconds>( write_end - write_start ).count();
        csvfile << to_string(length) << ',' << to_string(n_writes) << ',' << to_string(duration) << endl;
    }
}


void testBigStructWriting()
{
    unsigned int n_times = 1000;
    writeBigStruct(n_times);
    writeTenInts(n_times);
}

void writeBigStruct(unsigned int n_times)
{
    ofstream csvfile("data/bigstruct.csv");
    csvfile << "size of struct, time (nanoseconds)" << endl;

    gzFile file = gzopen("data/bigstructdump", "w");
    unsigned int length = sizeof(bigStruct);
    for(unsigned int i = 0; i < n_times; ++i)
    {
        bigStruct s;
        
        auto write_start = chrono::high_resolution_clock::now();
        gzwrite(file, &s, length);
        auto write_end = chrono::high_resolution_clock::now();
        long duration = chrono::duration_cast<chrono::nanoseconds>( write_end - write_start ).count();
        csvfile << to_string(length) << ',' << to_string(duration) << endl;
    }
    gzclose(file);
}

void writeTenInts(unsigned int n_times)
{
    ofstream csvfile("data/10_ints.csv");
    csvfile << "size of write, number of writes, time (nanoseconds)" << endl;
    gzFile file = gzopen("data/10_intsdump", "w");
    unsigned int length = sizeof(int16_t);
    unsigned int n_writes = 10;
    for(unsigned int i = 0; i < n_times; ++i)
    {
        int16_t a, b, c, d, e, f, g, h, ii, j;
        
        auto write_start = chrono::high_resolution_clock::now();
        
        gzwrite(file, &a, length);
        gzwrite(file, &b, length);
        gzwrite(file, &c, length);
        gzwrite(file, &d, length);
        gzwrite(file, &e, length);
        gzwrite(file, &f, length);
        gzwrite(file, &g, length);
        gzwrite(file, &h, length);
        gzwrite(file, &ii, length);
        gzwrite(file, &j, length);

        auto write_end = chrono::high_resolution_clock::now();
        long duration = chrono::duration_cast<chrono::nanoseconds>( write_end - write_start ).count();
        csvfile << to_string(length) << ',' << to_string(n_writes) << ',' << to_string(duration) << endl;
    }
}

void dynamicTest()
{
    unsigned int n_times = 1000;
    for(unsigned int i = 1; i <= 10; ++i)
    {
        writeWholeArray(i, n_times);
        writeEachCellOfArray(i, n_times);
    }
}

void writeWholeArray(const unsigned int length, unsigned int n_times)
{
    string filename("data/whole_" + to_string(length) + ".csv");
    ofstream csvfile(filename);
    csvfile << "size of write, time (nanoseconds)" << endl;
    unsigned int byteLength = sizeof(int16_t) * length;
    gzFile file = gzopen("data/arraydump", "w");
    for(unsigned int i = 0; i < n_times; ++i)
    {
        int16_t array [length];

        auto write_start = chrono::high_resolution_clock::now();
        gzwrite(file, array, byteLength);
        auto write_end = chrono::high_resolution_clock::now();
        long duration = chrono::duration_cast<chrono::nanoseconds>( write_end - write_start ).count();
        csvfile << to_string(byteLength) << ',' << to_string(duration) << endl;
    }
    gzclose(file);
}

void writeEachCellOfArray(const unsigned int length, unsigned int n_times)
{
    string filename("data/each_" + to_string(length) + ".csv");
    ofstream csvfile(filename);
    csvfile << "size of write, number of writes, time (nanoseconds)" << endl;
    unsigned int byteLength = sizeof(int16_t) * length;
    gzFile file = gzopen("data/eachdump", "w");
    for(unsigned int i = 0; i < n_times; ++i)
    {
        int16_t array [length];

        auto write_start = chrono::high_resolution_clock::now();
        
        for(unsigned int j = 0; j < length; ++j)
        gzwrite(file, array, byteLength);
        
        auto write_end = chrono::high_resolution_clock::now();
        long duration = chrono::duration_cast<chrono::nanoseconds>( write_end - write_start ).count();
        csvfile << to_string(byteLength) << ',' << to_string(length) << ',' << to_string(duration) << endl;
    }
    gzclose(file);

}


void testHugeArrayWriting()
{
    unsigned int n = 1000;
    writeWholeArray(256, n);
    writeEachCellOfArray(256, n);
    
    writeWholeArray(512, n);
    writeEachCellOfArray(512, n);
    
    writeWholeArray(1024, n);
    writeEachCellOfArray(1024, n);
    
    writeWholeArray(1024*1024, n); 
}

