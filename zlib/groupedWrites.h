#pragma once

struct smallStruct {
    int16_t a;
    int16_t b;
    int16_t c;
};

struct bigStruct {
    int16_t a;
    int16_t b;
    int16_t c;
    int16_t d;
    int16_t e;
    int16_t f;
    int16_t g;
    int16_t h;
    int16_t i;
    int16_t j;
};

int main(int argc, char ** argv);

// Measuring average writing speed of small data.
void testSmallStructWriting();
void writeSmallStruct(unsigned int n_times);
void writeThreeInts(unsigned int n_times);

// Measuring average writing speed of larger data
void testBigStructWriting();
void writeBigStruct(unsigned int n_times);
void writeTenInts(unsigned int n_times);

// Measuring evolution of average speed writing according to data size
void dynamicTest();
void writeWholeArray(const unsigned int length, unsigned int n_times);
void writeEachCellOfArray(const unsigned int length, unsigned int n_times);

// Measuring writing speed for huge size
void testHugeArrayWriting();
